#!/bin/bash

verifica_comando_existe() {
  comando="$1"
  if [[ -z "$comando" ]]; then
    echo "comando não especificado" >&2
    exit 1
  fi
  if ! command -v "$comando" &>/dev/null; then
    echo "${comando} não encontrado" >&2
    exit 1
  fi
}

verifica_comando_existe "tmux"
verifica_comando_existe "nvim"
verifica_comando_existe "jupyter"

if [[ -n "$TMUX" ]]; then
  echo "rode o script fora do tmux" >&2
  exit 1
fi

tmux new -s projeto -d

tmux rename-window -t projeto neovim
tmux send-keys -t projeto "nvim" Escape C-l Enter

tmux new-window -t projeto
tmux rename-window -t projeto terminal

tmux new-window -t projeto
tmux rename-window -t projeto jupyter
tmux send-keys -t projeto "jupyter notebook" Escape C-l

tmux select-window -t projeto:1
tmux attach -t projeto
