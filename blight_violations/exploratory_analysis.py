# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.3
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Blight Violations: Exploratory Data Analysis
# ## Imports
# Run the following cell to import the code we'll use in this notebook:

# %%
import warnings

import pandas as pd
from eda import simple_graphs
from eda.general import percentage_non_zero, print_unique
from eda.inspect_columns import inspect_cities, inspect_state, inspect_violation_codes
from general.load_data import read_csv
from IPython.display import display
from missing_values import number_percentage as mv

# %%
# pylint: disable=expression-not-assigned,no-member
pd.set_option("display.max_rows", None)
warnings.filterwarnings("ignore")

# %% [markdown]
# ## Loading the Data Set
# ### Training Data

# %%
df_train = read_csv(
    "readonly",
    "train.csv",
    index_col="ticket_id",
    encoding="cp1252",
)
assert df_train is not None
df_train.head(15)

# %%
df_train.info()

# %% [markdown]
# ### Test Data

# %%
df_test = read_csv("readonly", "test.csv", index_col="ticket_id")
assert df_test is not None
df_test.head(15)

# %%
df_test.info()

# %% [markdown]
# ### Addresses

# %%
df_addresses = read_csv("readonly", "addresses.csv", index_col="ticket_id")
assert df_addresses is not None
df_addresses.head(15)

# %%
df_addresses.info()

# %% [markdown]
# ### Geolocation Data

# %%
df_latlons = read_csv("readonly", "latlons.csv")
assert df_latlons is not None
df_latlons.head(15)

# %%
df_latlons.info()

# %% [markdown]
# ## Missing Values
# ### Training Data

# %%
mv.number_missing_values(df_train, label="TRAINING DATA")

# %%
mv.percentage_missing_values(df_train, label="TRAINING DATA")

# %% [markdown]
# ### Test Data

# %%
mv.number_missing_values(df_test, label="TEST DATA")

# %%
mv.percentage_missing_values(df_test, label="TEST DATA")

# %% [markdown]
# ### Addresses

# %%
mv.number_missing_values(df_addresses, label="ADDRESSES")

# %%
mv.percentage_missing_values(df_addresses, label="ADDRESSES")

# %% [markdown]
# ### Geolocation Data

# %%
mv.number_missing_values(df_latlons, label="GEOLOCATION DATA")

# %%
mv.percentage_missing_values(df_latlons, label="GEOLOCATION DATA")

# %% [markdown]
# ## Training and Test Sets

# %% [markdown]
# ### Column: country

# %%
print_unique(df_train, "country", label="TRAINING SET", pretty_print=True)

# %%
# Training data: Number of entries with country != "USA"
print(len(df_train[df_train.country != "USA"]))

# %%
print_unique(df_test, "country", label="TEST SET", pretty_print=True)

# %% [markdown]
# ### Column: state

# %%
# print_unique(df_train, "state", label="TRAINING SET", pretty_print=True)

# %%
100 * df_train.state.value_counts(normalize=True)

# %%
# print_unique(df_test, "state", label="TEST SET", pretty_print=True)

# %%
100 * df_test.state.value_counts(normalize=True)

# %%
# British Columbia
inspect_state(df_train, "BC")

# %%
# Berlin
inspect_state(df_train, "BL")

# %%
# Ontario
inspect_state(df_train, "ON")

# %%
# Quebec
inspect_state(df_train, "QC")

# %%
# Queensland
inspect_state(df_train, "QL")

# %%
# NOT United Kingdom! Ontario!
inspect_state(df_train, "UK")

# %%
tmp_df = df_train[df_train.state.isna()].loc[:, ["city"]]
tmp_df

# %%
tmp_df = df_test[df_test.state.isna()].loc[:, ["city"]]
tmp_df

# %%
del tmp_df

# %% [markdown]
# ### Column: city

# %%
inspect_cities(df_train, min_perc=0.25)

# %%
inspect_cities(df_test, min_perc=0.25)

# %% [markdown]
# ### Column: agency_name

# %%
print_unique(df_train, "agency_name", label="TRAINING SET", pretty_print=True)

# %%
100 * df_train.agency_name.value_counts(normalize=True)

# %%
print_unique(df_test, "agency_name", label="TEST SET", pretty_print=True)

# %%
100 * df_test.agency_name.value_counts(normalize=True)

# %% [markdown]
# ### Column: violation_code

# %%
print_unique(df_train, "violation_code", label="TRAINING SET", pretty_print=True)

# %%
print_unique(df_test, "violation_code", label="TEST SET", pretty_print=True)

# %%
pd.set_option("display.max_colwidth", None)

# %%
inspect_violation_codes(df_train, startswith="9-1-", n=5)

# %%
# Grafitti-related violations
# Maybe we can use this data to fix the column named "grafitti_status".
inspect_violation_codes(df_train, startswith="9-1-111", n=5)

# %%
# Waste-related violations
inspect_violation_codes(df_train, startswith="22-", n=5)

# %%
inspect_violation_codes(df_train, startswith="61-", n=5)

# %%
inspect_violation_codes(df_train, endswith="901", n=5)

# %%
pd.set_option("display.max_colwidth", 50)

# %%
tmp = df_train.violation_code.loc[df_train.violation_code.str.startswith("9-")]
display(100 * tmp.value_counts(normalize=True))
del tmp

# %% [markdown]
# ### Column: grafitti_status

# %%
print_unique(df_test, "grafitti_status", label="TEST SET", pretty_print=True)

# %%
tmp_df = df_test[["violation_code", "grafitti_status"]]
tmp_df = tmp_df[tmp_df.grafitti_status.notna()]
tmp_df.head(15)

# %%
print_unique(tmp_df, "violation_code", pretty_print=True)

# %%
tmp_df = df_test[["violation_code", "grafitti_status"]]
tmp_df = tmp_df[tmp_df.violation_code.str.startswith("9-1-111")]
display(tmp_df.head(15))
del tmp_df

# %% [markdown]
# ### Column: disposition

# %%
print_unique(df_train, "disposition", label="TRAINING SET", pretty_print=True)

# %%
tmp_df = df_train[df_train.compliance.isna()].loc[:, ["disposition"]]
print_unique(tmp_df, "disposition", pretty_print=True)
del tmp_df

# %%
tmp_df = df_train[df_train.compliance.notna()].loc[:, ["disposition"]]
print_unique(tmp_df, "disposition", pretty_print=True)

# %%
display(100 * tmp_df.disposition.value_counts(normalize=True))
del tmp_df

# %%
# Test data doesn't contain "Not responsible" cases!
print_unique(df_test, "disposition", label="TEST SET", pretty_print=True)

# %% [markdown]
# ### Column: fine_amount

# %%
simple_graphs.create_boxplot(df_train, "fine_amount", "Fine Amount")

# %%
df_train.fine_amount.describe()

# %%
simple_graphs.create_boxplot(df_train, "fine_amount", "Fine Amount", show_outliers=False)

# %%
simple_graphs.create_histplot(
    df_train,
    "fine_amount",
    "Fine Amount",
    remove_outliers=True,
)

# %% [markdown]
# ### Column: admin_fee

# %%
simple_graphs.create_boxplot(df_train, "admin_fee", "Admin Fee")

# %%
df_train.admin_fee.describe()

# %%
simple_graphs.create_boxplot(df_train, "admin_fee", "Admin Fee", show_outliers=False)

# %%
simple_graphs.create_histplot(
    df_train,
    "admin_fee",
    "Admin Fee",
    remove_outliers=True,
)

# %% [markdown]
# ### Column: state_fee

# %%
simple_graphs.create_boxplot(df_train, "state_fee", "State Fee")

# %%
df_train.state_fee.describe()

# %%
simple_graphs.create_boxplot(df_train, "state_fee", "State Fee", show_outliers=False)

# %%
simple_graphs.create_histplot(
    df_train,
    "state_fee",
    "State Fee",
    remove_outliers=True,
)

# %% [markdown]
# ### Column: late_fee

# %%
simple_graphs.create_boxplot(df_train, "late_fee", "Late Fee")

# %%
df_train.late_fee.describe()

# %%
simple_graphs.create_boxplot(df_train, "late_fee", "Late Fee", show_outliers=False)

# %%
simple_graphs.create_histplot(
    df_train,
    "late_fee",
    "Late Fee",
    remove_outliers=True,
)

# %% [markdown]
# ### Column: discount_amount

# %%
simple_graphs.create_boxplot(df_train, "discount_amount", "Discount Amount")

# %%
df_train.discount_amount.describe()

# %%
simple_graphs.create_boxplot(df_train, "discount_amount", "Discount Amount", show_outliers=False)

# %%
percentage_non_zero(df_train, "discount_amount")

# %% [markdown]
# ### Column: clean_up_cost

# %%
percentage_non_zero(df_train, "clean_up_cost")

# %% [markdown]
# ### Column: judgment_amount

# %%
simple_graphs.create_boxplot(df_train, "judgment_amount", "Judgment Amount")

# %%
df_train.judgment_amount.describe()

# %%
simple_graphs.create_boxplot(df_train, "judgment_amount", "Judgment Amount", show_outliers=False)

# %%
simple_graphs.create_histplot(
    df_train,
    "judgment_amount",
    "Judgment Amount",
    remove_outliers=True,
)

# %%
# Only violators that were found "Responsible".
cols = [
    "fine_amount",
    "admin_fee",
    "state_fee",
    "late_fee",
    "discount_amount",
    "judgment_amount",
]
tmp_df = df_train.loc[df_train.compliance.notna(), cols].dropna()
del cols

# %%
# "Responsible" violators were always charged these fees:
print((tmp_df["admin_fee"] > 0).all())
print((tmp_df["state_fee"] > 0).all())

# %%
# Not always charged:
print((tmp_df["fine_amount"] > 0).all())
print((tmp_df["late_fee"] > 0).all())

# %%
# Relation between "judgment_amount" and the other numerical features?
tmp_df["sum"] = tmp_df.fine_amount + tmp_df.admin_fee + tmp_df.state_fee + tmp_df.late_fee
display(tmp_df[["sum", "judgment_amount"]].head(15))
print((tmp_df["sum"] == tmp_df.judgment_amount).all())

# %%
display(tmp_df.loc[tmp_df["sum"] != tmp_df.judgment_amount, :].head(15))
del tmp_df
