import matplotlib.pyplot as plt
from sklearn.metrics import (
    ConfusionMatrixDisplay,
    accuracy_score,
    auc,
    classification_report,
    confusion_matrix,
    plot_precision_recall_curve,
    plot_roc_curve,
    precision_recall_curve,
    precision_score,
    recall_score,
    roc_auc_score,
)


def elements_confusion_matrix(y_test, y_pred, label=None):
    """Compute and print the elements of the confusion matrix."""
    cm = confusion_matrix(y_test, y_pred)
    tn, fp, fn, tp = cm.ravel()
    if label is not None:
        print(label)
    print(f"True Negatives:  {tn}")
    print(f"False Positives: {fp}")
    print(f"False Negatives: {fn}")
    print(f"True Positives:  {tp}")


def plot_confusion_matrix(y_test, y_pred, class_names, label=None):
    _, ax = plt.subplots(figsize=(12, 8))
    ConfusionMatrixDisplay.from_predictions(
        y_test,
        y_pred,
        display_labels=class_names,
        ax=ax,
    )
    title = "Confusion Matrix"
    if label is not None:
        title = f"{label}: {title}"
    ax.set_title(title)
    plt.show()


def compute_print_metric(metric_name, metric_func, y_test, y_pred, label=None):
    """Helper function for computing and printing some of the metrics."""
    metric_value = metric_func(y_test, y_pred)
    if label is not None:
        print(label)
    print(f"{metric_name}: {metric_value:.3f}")


def accuracy(y_test, y_pred, label=None):
    """Compute and print the accuracy."""
    metric_name = "Accuracy"
    metric_func = accuracy_score
    compute_print_metric(metric_name, metric_func, y_test, y_pred, label)


def precision(y_test, y_pred, label=None):
    """Compute and print the precision."""
    metric_name = "Precision"
    metric_func = precision_score
    compute_print_metric(metric_name, metric_func, y_test, y_pred, label)


def recall(y_test, y_pred, label=None):
    """Compute and print the recall."""
    metric_name = "Recall"
    metric_func = recall_score
    compute_print_metric(metric_name, metric_func, y_test, y_pred, label)


def print_classification_report(y_test, y_pred, class_names, label=None):
    """Print a report containing several important classification metrics."""
    cr = classification_report(y_test, y_pred, target_names=class_names)
    if label is not None:
        print(label)
    print(cr)


def precision_recall_plot(clf, x_test, y_test, label=None):
    _, ax = plt.subplots(figsize=(12, 8))
    plot_precision_recall_curve(clf, x_test, y_test, ax=ax)
    title = "Precision-Recall Curve"
    if label is not None:
        title = f"{label}: {title}"
    ax.set_title(title)
    plt.show()


def precision_recall_auc(y_test, probs, label=None):
    precision, recall, _ = precision_recall_curve(y_test, probs)
    area_under_curve = auc(recall, precision)
    if label is not None:
        print(label)
    print(f"Precision-Recall AUC: {area_under_curve:.3f}")


def roc_plot(clf, x_test, y_test, label=None):
    _, ax = plt.subplots(figsize=(12, 8))
    plot_roc_curve(clf, x_test, y_test, ax=ax)
    title = "ROC Curve"
    if label is not None:
        title = f"{label}: {title}"
    ax.set_title(title)
    plt.show()


def roc_auc(y_test, probs, label=None):
    area_under_curve = roc_auc_score(y_test, probs)
    if label is not None:
        print(label)
    print(f"ROC AUC: {area_under_curve:.3f}")
