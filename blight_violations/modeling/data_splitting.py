from sklearn.model_selection import train_test_split


def split_target_features(df_train, target, features=None):
    """Separate features from the target."""
    y = df_train.loc[:, target]
    if features is None:
        features = [column for column in df_train.columns.to_list() if column != target]
    x = df_train.loc[:, features]
    return x, y


def split_train_cv(df_train, target, features=None, ratio=0.25):
    """Use a portion of the training data to create a cross validation set."""
    x, y = split_target_features(df_train, target, features)
    x_train, x_cv, y_train, y_cv = train_test_split(x, y, test_size=ratio, random_state=0)
    return x_train, x_cv, y_train, y_cv
