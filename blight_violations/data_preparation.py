# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.3
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Blight Violations: Data Preparation
# ## Imports
# Run the following cell to import the code we'll use in this notebook:

# %%
import numpy as np
import pandas as pd
from general.load_data import read_csv

# %%
# pylint: disable=expression-not-assigned,no-member
pd.set_option("display.max_rows", None)

# %% [markdown]
# ## Loading the Data Set

# %%
# Training Data
df_train = read_csv("readonly", "train.csv", index_col="ticket_id", encoding="cp1252")

# Test Data
df_test = read_csv("readonly", "test.csv", index_col="ticket_id")

# %% [markdown]
# ## Remove Columns

# %%
columns_to_drop = [
    "inspector_name",
    "violation_zip_code",
    "mailing_address_str_number",
    "mailing_address_str_name",
    "city",
    "zip_code",
    "non_us_str_code",
    "violation_description",
    "disposition",
    "fine_amount",
    "admin_fee",
    "state_fee",
    "late_fee",
    "clean_up_cost",
]
df_train = df_train.drop(columns=columns_to_drop)
df_test = df_test.drop(columns=columns_to_drop)
del columns_to_drop

columns_to_drop_train = [
    "payment_amount",
    "payment_date",
    "payment_status",
    "balance_due",
    "collection_status",
    "compliance_detail",
]
df_train = df_train.drop(columns=columns_to_drop_train)
del columns_to_drop_train

# %% [markdown]
# ## Compliance
# It doesn't make sense to keep entries for which the violator was found "Not
# responsible". Then we remove such records.

# %%
df_train = df_train[df_train.compliance.notna()]
df_train["compliance"] = df_train["compliance"].astype(np.ushort)

# %% [markdown]
# ## Agency

# %%
agency_dict = {
    "Buildings, Safety Engineering & Env Department": 0,
    "Department of Public Works": 1,
    "Detroit Police Department": 2,
    "Health Department": 2,
    "Neighborhood City Halls": 2,
}

df_train["agency"] = df_train["agency_name"].map(agency_dict).astype(np.ushort)
df_train = df_train.drop(columns="agency_name")

df_test["agency"] = df_test["agency_name"].map(agency_dict).astype(np.ushort)
df_test = df_test.drop(columns="agency_name")

del agency_dict

# %% [markdown]
# ## Is the violator in Michigan?

# %%
df_train["state"] = df_train["state"].fillna(value="<Unknown>")
df_train["is_in_mi"] = df_train["state"].apply(lambda x: 1 if x == "MI" else 0).astype(np.ushort)

df_test["state"] = df_test["state"].fillna(value="<Unknown>")
df_test["is_in_mi"] = df_test["state"].apply(lambda x: 1 if x == "MI" else 0).astype(np.ushort)

# %% [markdown]
# ## Is the violator in the US?

# %%
df_train["country"] = (
    df_train["country"]
    .replace(to_replace="Aust", value="Not USA")
    .replace(to_replace="Cana", value="Not USA")
    .replace(to_replace="Egyp", value="Not USA")
    .replace(to_replace="Germ", value="Not USA")
)

df_train.loc[df_train.state == "<Unknown>", "country"] = "Not USA"
df_test.loc[df_test.state == "<Unknown>", "country"] = "Not USA"

non_us_states = ["BC", "BL", "ON", "QC", "QL", "UK"]

df_train.loc[df_train.state.isin(non_us_states), "country"] = "Not USA"
df_train = df_train.drop(columns="state")

df_test.loc[df_test.state.isin(non_us_states), "country"] = "Not USA"
df_test = df_test.drop(columns="state")

del non_us_states

df_train["is_in_us"] = df_train.country.apply(lambda x: 1 if x == "USA" else 0).astype(np.ushort)
df_train = df_train.drop(columns="country")

df_test["is_in_us"] = df_test.country.apply(lambda x: 1 if x == "USA" else 0).astype(np.ushort)
df_test = df_test.drop(columns="country")

# %% [markdown]
# ## Grafitti Status

# %%
df_train["grafitti_status"] = df_train["grafitti_status"].fillna(value="<Unknown>")
df_train.loc[
    df_train.violation_code.str.startswith("9-1-111"), "grafitti_status"
] = "GRAFFITI TICKET"
df_train["grafitti_status"] = (
    df_train["grafitti_status"]
    .apply(lambda x: 1 if x == "GRAFFITI TICKET" else 0)
    .astype(np.ushort)
)

df_test["grafitti_status"] = df_test["grafitti_status"].fillna(value="<Unknown>")
df_test.loc[
    df_test.violation_code.str.startswith("9-1-111"), "grafitti_status"
] = "GRAFFITI TICKET"
df_test["grafitti_status"] = (
    df_test["grafitti_status"]
    .apply(lambda x: 1 if x == "GRAFFITI TICKET" else 0)
    .astype(np.ushort)
)

# %% [markdown]
# ## Violation Code

# %%
df_train.loc[df_train.violation_code.str.endswith("901"), "violation_code"] = "901"
df_train.loc[df_train.violation_code.str.startswith("22-"), "violation_code"] = "22"
df_train.loc[df_train.violation_code.str.startswith("61-"), "violation_code"] = "61"
df_train.loc[df_train.violation_code.str.startswith("9-1-36"), "violation_code"] = "36"
df_train.loc[df_train.violation_code.str.startswith("9-1-81"), "violation_code"] = "81"
df_train.loc[df_train.violation_code.str.startswith("9-1-104"), "violation_code"] = "104"
df_train.loc[df_train.violation_code.str.startswith("9-1-"), "violation_code"] = "9"
df_train["violation_code_cat"] = (
    df_train["violation_code"].astype("category").cat.codes.astype(np.ushort)
)
df_train = df_train.drop(columns="violation_code")

df_test.loc[df_test.violation_code.str.endswith("901"), "violation_code"] = "901"
df_test.loc[df_test.violation_code.str.startswith("22-"), "violation_code"] = "22"
df_test.loc[df_test.violation_code.str.startswith("61-"), "violation_code"] = "61"
df_test.loc[df_test.violation_code.str.startswith("9-1-36"), "violation_code"] = "36"
df_test.loc[df_test.violation_code.str.startswith("9-1-81"), "violation_code"] = "81"
df_test.loc[df_test.violation_code.str.startswith("9-1-104"), "violation_code"] = "104"
df_test.loc[df_test.violation_code.str.startswith("9-1-"), "violation_code"] = "9"
df_test["violation_code_cat"] = (
    df_test["violation_code"].astype("category").cat.codes.astype(np.ushort)
)
df_test = df_test.drop(columns="violation_code")

# %% [markdown]
# ## Total Charge
# Total Charge = Fine + Fees - Discount

# %%
df_train["total_charge"] = df_train["judgment_amount"] - df_train["discount_amount"]
df_train = df_train.drop(columns=["judgment_amount", "discount_amount"])

df_test["total_charge"] = df_test["judgment_amount"] - df_test["discount_amount"]
df_test = df_test.drop(columns=["judgment_amount", "discount_amount"])

# %%
# Discrete version of this feature:
def discretize_total_charge(total_charge):
    if total_charge < 100:
        return 0
    if 100 <= total_charge < 500:
        return 1
    if 500 <= total_charge < 1000:
        return 2
    return 3


df_train["total_charge_cat"] = (
    df_train["total_charge"].apply(discretize_total_charge).astype(np.ushort)
)
df_test["total_charge_cat"] = (
    df_test["total_charge"].apply(discretize_total_charge).astype(np.ushort)
)

# %%
#
