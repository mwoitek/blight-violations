from numpy import ushort
from sklearn.preprocessing import OneHotEncoder


def one_hot_encode_feature(df, feature, drop=True):
    """One-hot encode a categorical feature."""
    ohe = OneHotEncoder()
    feature_one_hot = (
        ohe.fit_transform(df[feature].to_numpy().reshape(-1, 1)).toarray().astype(ushort)
    )
    for i in range(feature_one_hot.shape[1]):
        df[f"{feature}_{i}"] = feature_one_hot[:, i]
    if drop:
        df = df.drop(columns=feature)
    return df


def one_hot_encode_features(df, features, drop=True):
    """One-hot encode a set of categorical features."""
    for feature in features:
        df = one_hot_encode_feature(df, feature, drop=drop)
    return df
