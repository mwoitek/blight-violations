def remove_outliers_iqr_ndarray(df, column):
    vals = df[column].dropna()
    q1 = vals.quantile(q=0.25)
    q3 = vals.quantile(q=0.75)
    iqr = q3 - q1
    return vals[~((vals < q1 - 1.5 * iqr) | (vals > q3 + 1.5 * iqr))].to_numpy()
