from pathlib import Path

import pandas as pd


def get_data_folder(folder_name):
    """Get the path to the folder containing the data files."""
    data_folder = Path(".").resolve() / folder_name
    if data_folder.exists() and data_folder.is_dir():
        return data_folder
    return None


def read_csv(folder_name, file_name, **kwargs):
    """Create a Pandas DataFrame by reading a CSV file."""
    data_folder = get_data_folder(folder_name)
    if data_folder is None:
        return None

    file_path = data_folder / file_name
    if not file_path.exists() or not file_path.is_file():
        return None

    return pd.read_csv(file_path, **kwargs)
