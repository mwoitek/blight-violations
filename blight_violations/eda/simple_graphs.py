import matplotlib.pyplot as plt
import seaborn as sns
from preprocessing.outliers import remove_outliers_iqr_ndarray


def create_boxplot(df, column, ylabel=None, title=None, show_outliers=True, figsize=None):
    """Function for creating a boxplot."""
    if figsize is None:
        figsize = (12, 8)
    _, ax = plt.subplots(figsize=figsize)
    sns.boxplot(y=column, data=df, showfliers=show_outliers, ax=ax)
    ax.set_xticks([])
    if ylabel is None:
        ylabel = column
    ax.set_ylabel(ylabel)
    if title is None:
        title = f"{ylabel}: Boxplot"
        title += " (Outliers not shown)" if not show_outliers else ""
    ax.set_title(title)
    plt.show()


def create_histplot(
    df, column, xlabel=None, title=None, bins="auto", remove_outliers=False, figsize=None
):
    """Function for creating a histogram."""
    if figsize is None:
        figsize = (12, 8)
    _, ax = plt.subplots(figsize=figsize)
    if remove_outliers:
        x = remove_outliers_iqr_ndarray(df, column)
    else:
        x = df[column].dropna().to_numpy()
    sns.histplot(x=x, bins=bins, ax=ax)
    ax.set_ylabel("Count")
    if xlabel is None:
        xlabel = column
    ax.set_xlabel(xlabel)
    if title is None:
        title = f"{xlabel}: Histogram"
        title += " (Outliers were removed)" if remove_outliers else ""
    ax.set_title(title)
    plt.show()
