from IPython.display import display


def inspect_cities(df, min_perc=1):
    cities = df.city.dropna().str.lower()
    cities = (
        cities.replace(to_replace="det", value="detroit")
        .replace(to_replace="det.", value="detroit")
        .replace(to_replace="w. bloomfield", value="west bloomfield")
    )
    perc = 100 * cities.value_counts(normalize=True)
    perc = perc[perc >= min_perc]
    display(perc.apply(lambda x: f"{x:.2f}%"))
    print(f"Total: {perc.sum():.2f}%")


def inspect_state(df, state, n=15):
    tmp_df = df.loc[df.state == state, ["city", "state", "country"]]
    if len(tmp_df) > n:
        display(tmp_df.head(n))
    else:
        display(tmp_df)


def inspect_violation_codes(df, startswith=None, endswith=None, n=15):
    if startswith is not None:
        tmp_df = df[df.violation_code.str.startswith(startswith)]
    elif endswith is not None:
        tmp_df = df[df.violation_code.str.endswith(endswith)]
    tmp_df = tmp_df[["violation_code", "violation_description", "agency_name"]]
    display(tmp_df.head(n))
    display(tmp_df.tail(n))
