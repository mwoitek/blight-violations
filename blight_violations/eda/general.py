import numpy as np
from IPython.display import display


def percentage_non_zero(df, column, label=None):
    """Print the percentage of zero and non-zero values in a given column of the DataFrame."""
    if label is not None:
        print(label)
    print(f"Column: {column}")
    tmp = df[column].dropna().apply(lambda x: "Zero" if x == 0 else "Non-zero")
    display((100 * tmp.value_counts(normalize=True)).apply(lambda x: f"{x:.2f}%"))


def print_unique(df, column, label=None, pretty_print=False):
    """Print the unique values in a given column of the DataFrame."""
    if label is not None:
        print(label)
    print(f"Column: {column}")
    uniq = np.sort(df[column].dropna().unique())
    print(f"Number of unique values: {len(uniq)}")
    print("Unique values:")
    if pretty_print:
        for i, value in enumerate(uniq):
            print(f"Value {i + 1}: {value}")
    else:
        print(uniq)
